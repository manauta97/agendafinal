package Interfaces;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import Codigo.IoDatos;
import Interfaces.Login.BtnNewButtonMouseListener;
import Interfaces.Login.BtnRegistrarseMouseListener;
import Interfaces.Login.PasswordFocusListener;
import Interfaces.Login.TxtNombreFocusListener;

public class Login extends JFrame {

	private JPanel contentPane;
	private JLabel lblicono;
	private JTextField txtNombre;
	private JLabel lblNombreusuario;
	private JLabel lblContrasea;
	private JPasswordField password;
	private JButton btnEntrar;
	private JLabel lblNewLabel;
	private JButton btnRegistrarse;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 338, 473);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblicono = new JLabel("datos");
		lblicono.setBackground(Color.YELLOW);
		lblicono.setHorizontalAlignment(SwingConstants.CENTER);
		lblicono.setIcon(new ImageIcon(".\\img\\icons8-usuario-masculino-en-c\u00EDrculo-100.png"));
		lblicono.setBounds(121, 48, 90, 86);
		contentPane.add(lblicono);
		lblicono.setFocusable(true);
		lblicono.requestFocus();

		txtNombre = new JTextField();
		txtNombre.setFont(new Font("Segoe Print", Font.BOLD, 12));
		txtNombre.addFocusListener(new TxtNombreFocusListener());
		txtNombre.setText("Usuario");
		txtNombre.setToolTipText("Nombre\r\n");
		txtNombre.setForeground(Color.GRAY);
		txtNombre.setBounds(100, 175, 171, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);

		lblNombreusuario = new JLabel("");
		lblNombreusuario.setIcon(new ImageIcon(".\\img\\nombre.png"));
		lblNombreusuario.setHorizontalAlignment(SwingConstants.CENTER);
		lblNombreusuario.setBounds(39, 175, 63, 32);
		contentPane.add(lblNombreusuario);

		lblContrasea = new JLabel("");
		lblContrasea.setIcon(new ImageIcon(".\\img\\contrasena.png"));
		lblContrasea.setHorizontalAlignment(SwingConstants.CENTER);
		lblContrasea.setBounds(39, 218, 63, 20);
		contentPane.add(lblContrasea);

		password = new JPasswordField();
		password.addFocusListener(new PasswordFocusListener());
		password.setText("contraseņa");
		password.setBounds(100, 218, 171, 20);
		contentPane.add(password);

		btnEntrar = new JButton("");
		btnEntrar.addMouseListener(new BtnNewButtonMouseListener());
		btnEntrar.setIcon(new ImageIcon(".\\img\\iniciar-sesion.png"));
		btnEntrar.setBounds(39, 286, 63, 51);
		contentPane.add(btnEntrar);

		btnRegistrarse = new JButton("");
		btnRegistrarse.addMouseListener(new BtnRegistrarseMouseListener());
		btnRegistrarse.setIcon(new ImageIcon(".\\img\\nota.png"));
		btnRegistrarse.setBounds(206, 286, 65, 51);
		contentPane.add(btnRegistrarse);
		// btnEntrar.setEnabled(false);

		lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setIcon(new ImageIcon(".\\img\\8321_320_480.jpg"));
		lblNewLabel.setBounds(0, 0, 322, 434);
		contentPane.add(lblNewLabel);

	}

	public class BtnNewButtonMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			String nombre = txtNombre.getText();
			char[] vChar = password.getPassword();
			String contraseņa = new String(vChar);

			if (IoDatos.comprobarlogin(txtNombre.getText(), contraseņa)) {
				MenuUsu menu = new MenuUsu(nombre);
				menu.setVisible(true);
				dispose();

			} else {
				JOptionPane.showMessageDialog(null, "Usuario no encontrado", "Mensaje", JOptionPane.WARNING_MESSAGE);
				txtNombre.setText("");
				password.setText("");
			}
			
				IoDatos.CargarContacto(txtNombre.getText());
			
			

		}
	}

	public class TxtNombreFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent arg0) {

			txtNombre.setText("");
		}

		@Override
		public void focusLost(FocusEvent e) {
			comprobarboton();

		}
	}

	public class PasswordFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent arg0) {
			password.setText("");
		}
	}

	public class BtnRegistrarseMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {

			AņadirUsu au = new AņadirUsu();
			au.setVisible(true);
			dispose();
		}
	}

	private void comprobarboton() {
		Boolean n = true;

		if (txtNombre.getText().equalsIgnoreCase("juan")) {
			n = true;
		} else {
			n = false;
		}

	}
}
