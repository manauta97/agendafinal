package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import Codigo.Contacto;
import Codigo.IoDatos;
import Interfaces.MenuUsu.BtnAñadirMouseListener;
import Interfaces.MenuUsu.BtnEliminarMouseListener;
import Interfaces.MenuUsu.BtnNewButtonMouseListener;
import Interfaces.MenuUsu.BtnVerContactosMouseListener;

public class MenuUsu extends JFrame {

	private JPanel contentPane;
	private JLabel lblHola;
	private String nombre;
	private JButton btnAñadir;
	private JButton btnEliminar;
	private JButton btnVerContactos;
	private ArrayList<Contacto> vContactos;
	private JTextField txteliminar;
	private JButton btnNewButton;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuUsu frame = new MenuUsu("");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MenuUsu(String nombre) {
		this.nombre = nombre;
		vContactos = IoDatos.CargarContacto(nombre);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 469, 310);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblHola = new JLabel("Hola " + nombre);
		lblHola.setHorizontalAlignment(SwingConstants.CENTER);
		lblHola.setBounds(46, 34, 347, 30);
		contentPane.add(lblHola);

		btnAñadir = new JButton("A\u00F1adir Contacto");
		btnAñadir.addMouseListener(new BtnAñadirMouseListener());
		btnAñadir.setBounds(240, 91, 153, 30);
		contentPane.add(btnAñadir);

		btnEliminar = new JButton("Eliminar Contacto");
		btnEliminar.addMouseListener(new BtnEliminarMouseListener());
		btnEliminar.setBounds(46, 123, 153, 30);
		contentPane.add(btnEliminar);

		btnVerContactos = new JButton("Ver Contactos");
		btnVerContactos.addMouseListener(new BtnVerContactosMouseListener());
		btnVerContactos.setBounds(46, 164, 153, 30);
		contentPane.add(btnVerContactos);

		txteliminar = new JTextField();
		txteliminar.setBounds(46, 91, 153, 25);
		contentPane.add(txteliminar);
		txteliminar.setColumns(10);

		btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new BtnNewButtonMouseListener());
		btnNewButton.setIcon(new ImageIcon(".\\img\\regreso.png"));
		btnNewButton.setBounds(328, 156, 65, 38);
		contentPane.add(btnNewButton);
		
		label = new JLabel("");
		label.setBounds(46, 205, 347, 44);
		contentPane.add(label);
	}

	public class BtnAñadirMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {

			AñadirContac añadirC = new AñadirContac(vContactos);
			añadirC.setVisible(true);

		}
	}

	public class BtnEliminarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			
			IoDatos.eliminarContacto(vContactos, txteliminar.getText());

		}
	}

	public class BtnVerContactosMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			IoDatos.CargarContacto(nombre);
		}
	}

	public class BtnNewButtonMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			IoDatos.guardarContacto(nombre,vContactos);
			Login sl = new Login();
			sl.setVisible(true);
			dispose();
		}
	}

}
