package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Codigo.Contacto;
import Codigo.IoDatos;
import Interfaces.AņadirUsu.BtnNewButtonMouseListener;
import Interfaces.AņadirUsu.BtnRegistrarseMouseListener;

public class AņadirUsu extends JFrame {

	private JPanel contentPane;
	private JTextField txtnombre;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnRegistrarse;
	private JPasswordField password;
	private JButton btnNewButton;
	private ArrayList<Contacto> vUsuarios;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AņadirUsu frame = new AņadirUsu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AņadirUsu() {
		vUsuarios = new ArrayList<Contacto>();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 263, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		txtnombre = new JTextField();
		txtnombre.setBounds(125, 56, 86, 20);
		contentPane.add(txtnombre);
		txtnombre.setColumns(10);

		lblNewLabel = new JLabel("Nombre");
		lblNewLabel.setBounds(35, 59, 46, 14);
		contentPane.add(lblNewLabel);

		lblNewLabel_1 = new JLabel("Contrase\u00F1a");
		lblNewLabel_1.setBounds(35, 90, 71, 14);
		contentPane.add(lblNewLabel_1);

		btnRegistrarse = new JButton("Registrarse");
		btnRegistrarse.addMouseListener(new BtnRegistrarseMouseListener());
		btnRegistrarse.setBounds(102, 138, 109, 23);
		contentPane.add(btnRegistrarse);

		password = new JPasswordField();
		password.setBounds(125, 87, 86, 20);
		contentPane.add(password);

		btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new BtnNewButtonMouseListener());
		btnNewButton.setIcon(new ImageIcon(".\\img\\regreso.png"));
		btnNewButton.setBounds(10, 202, 54, 31);
		contentPane.add(btnNewButton);
	}

	public class BtnRegistrarseMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			char[] vChar = password.getPassword();
			String contraseņa = new String(vChar);

			IoDatos.guardarUsuario(txtnombre.getText(), contraseņa);
			JOptionPane.showMessageDialog(null, "Usuario Guardado", "Mensaje", JOptionPane.WARNING_MESSAGE);
			txtnombre.setText("");
			password.setText("");
		}
	}

	public class BtnNewButtonMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {

			Login pl = new Login();
			pl.setVisible(true);
			dispose();
		}
	}

}
