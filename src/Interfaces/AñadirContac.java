package Interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import Codigo.Contacto;
import Interfaces.AņadirContac.BtnGuardarMouseListener;
import Interfaces.AņadirContac.BtnNewButtonMouseListener;

public class AņadirContac extends JFrame {

	private JPanel contentPane;
	private JLabel lblNombre;
	private JTextField txtnombrec;
	private JLabel lblContrasea;
	private JTextField txttlfc;
	private JButton btnGuardar;
	private ArrayList<Contacto> vContactos;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AņadirContac frame = new AņadirContac(null);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AņadirContac(ArrayList<Contacto> vContactos) {

		this.vContactos = vContactos;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 242, 282);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 34, 70, 20);
		contentPane.add(lblNombre);

		txtnombrec = new JTextField();
		txtnombrec.setBounds(81, 34, 86, 20);
		contentPane.add(txtnombrec);
		txtnombrec.setColumns(10);

		lblContrasea = new JLabel("Telefono");
		lblContrasea.setBounds(10, 71, 91, 20);
		contentPane.add(lblContrasea);

		txttlfc = new JTextField();
		txttlfc.setBounds(81, 71, 86, 20);
		contentPane.add(txttlfc);
		txttlfc.setColumns(10);

		btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new BtnGuardarMouseListener());
		btnGuardar.setBounds(78, 117, 89, 23);
		contentPane.add(btnGuardar);

		btnNewButton = new JButton("");
		btnNewButton.addMouseListener(new BtnNewButtonMouseListener());
		btnNewButton.setIcon(new ImageIcon(".\\img\\regreso.png"));
		btnNewButton.setBounds(10, 197, 70, 34);
		contentPane.add(btnNewButton);
	}

	public class BtnGuardarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {

			vContactos.add(new Contacto(txtnombrec.getText(), txttlfc.getText()));
			JOptionPane.showMessageDialog(null, "Contacto Guardado", "Mensaje", JOptionPane.WARNING_MESSAGE);
			System.out.println(vContactos);
		}
	}

	public class BtnNewButtonMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			dispose();
		}
	}

}
