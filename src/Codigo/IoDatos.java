package Codigo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class IoDatos {
	public static boolean comprobarlogin(String nombre, String contraseņa) {
		File fichero = new File("datos/usuario.txt");
		boolean passwordCorrecto = false;

		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			Scanner leer = new Scanner(fichero);
			while (leer.hasNext()) {
				String[] linea = leer.nextLine().split("-");
				if (linea[0].equalsIgnoreCase(nombre) && linea[1].equalsIgnoreCase(contraseņa)) {
					passwordCorrecto = true;
					break;
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return passwordCorrecto;
	}

	public static void guardarUsuario(String nombre, String contraseņa) {
		File fichero = new File("datos/usuario.txt");
		PrintWriter pw = null;
		FileWriter fw = null;

		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			fw = new FileWriter(fichero, true);
			pw = new PrintWriter(fw);

			pw.println(nombre + "-" + contraseņa);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			if (pw != null)
				pw.close();
			if (fw != null)
				fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ArrayList<Contacto> CargarContacto(String usuario) {
		ArrayList<Contacto> vContactos = new ArrayList();

		File f = new File(usuario);
		FileInputStream fi = null;
		ObjectInputStream leer = null;
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {

			}
		}

		try {
			fi = new FileInputStream(f);
			leer = new ObjectInputStream(fi);

			vContactos = (ArrayList<Contacto>) leer.readObject();

		} catch (IOException | ClassNotFoundException e) {

		}
		return vContactos;
	}

	public static void guardarContacto(String usuario, ArrayList<Contacto> vContactos) {
		File fichero = new File("datos/" + usuario + ".dat");
		FileOutputStream fis = null;
		ObjectOutputStream obj = null;

		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			fis = new FileOutputStream(fichero);
			obj = new ObjectOutputStream(fis);

			obj.writeObject(vContactos);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {

				obj.close();
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void eliminarContacto(ArrayList<Contacto> vContactos, String nombre) { 
		File fichero = new File("datos/nombre.txt");

		if (!fichero.exists()) {
			try {
				fichero.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			Scanner leer = new Scanner(fichero);
			while (leer.hasNextLine()) {
				for (int i = 0; i < vContactos.size(); i++) {
					if (vContactos.get(i).getNombre().equalsIgnoreCase(nombre)) {
						vContactos.remove(i);
					}
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
